package com.tdstudio.infinitytraffic.ggmap;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by TuDLT on 5/22/2018.
 */
public class Route {
    public List<LatLng> points;

    public String startAddress;
    public LatLng startLocation;

    public String endAddress;
    public LatLng endLocation;
}
