package com.tdstudio.infinitytraffic.initApi.core;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.tdstudio.infinitytraffic.BuildConfig;
import com.tdstudio.infinitytraffic.initApi.services.AuthService;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bapvn on 14/08/2018.
 */

public class ApiAuth {

    private static final String TAG = ApiAuth.class.getSimpleName();
    private static final long TIME_OUT = 300000;
    private static final String APP_VERSION = "version";
    private static final String APP_PLATFORM = "platform";
    private static ApiAuth sInstance;
    private volatile AuthService mApiService;

    /**
     * constructor
     */
    private ApiAuth() {
        // no instance
    }

    /**
     * Api client
     *
     * @return
     */
    public synchronized static ApiAuth getInstance() {
        if (sInstance == null) {
            sInstance = new ApiAuth();
        }
        return sInstance;
    }

    /**
     * Service
     *
     * @return
     */
    public synchronized static AuthService getService() {
        return getInstance().mApiService;
    }

    /**
     * init confirm url
     *
     * @param config
     */
    public void init(ApiConfig config) {
        Log.d(TAG, "initAuth: " + config);
        // Author
        final String auth = config.getAuth();
        final String verifyToken = config.getToken();
        // init
        BooleanAdapter booleanAdapter = new BooleanAdapter();
        IntegerAdapter integerAdapter = new IntegerAdapter();
        DoubleAdapter doubleAdapter = new DoubleAdapter();
        // init Gson
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Boolean.class, booleanAdapter)
                .registerTypeAdapter(boolean.class, booleanAdapter)
                .registerTypeAdapter(Integer.class, integerAdapter)
                .registerTypeAdapter(int.class, integerAdapter)
                .registerTypeAdapter(Double.class, doubleAdapter)
                .registerTypeAdapter(double.class, doubleAdapter)
                .disableHtmlEscaping()
                .create();
        // init OkHttpClient
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient().newBuilder();
        okHttpBuilder.connectTimeout(TIME_OUT, TimeUnit.MILLISECONDS);
        okHttpBuilder.interceptors().add(new ForbiddenInterceptor());

        // Log
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpBuilder.interceptors().add(logInterceptor);
        }
        // AUTHORIZATION
        okHttpBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder()
                    .addHeader(APP_VERSION, BuildConfig.VERSION_NAME)
                    .addHeader(APP_PLATFORM, "2")
                    .addHeader("Accept-Language", Locale.getDefault().getLanguage())
                    .method(original.method(), original.body());
            if (verifyToken != null) {
                requestBuilder.addHeader("token", verifyToken);
            }
            Request request = requestBuilder.build();
            return chain.proceed(request);
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(config.getBaseUrl())
                .client(okHttpBuilder.build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        mApiService = retrofit.create(AuthService.class);
    }
}