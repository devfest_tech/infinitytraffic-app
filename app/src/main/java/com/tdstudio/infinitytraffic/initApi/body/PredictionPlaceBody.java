package com.tdstudio.infinitytraffic.initApi.body;

import lombok.Data;

@Data
public class PredictionPlaceBody {
    private String input;
    private String location;
    private int radius;
    private String key;
    private String types;
}
