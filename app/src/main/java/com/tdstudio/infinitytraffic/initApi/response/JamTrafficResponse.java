package com.tdstudio.infinitytraffic.initApi.response;

import com.tdstudio.infinitytraffic.model.JamModel;

import java.util.List;

import lombok.Data;

@Data
public class JamTrafficResponse extends BaseResponse {
    List<JamModel> data;
}
