package com.tdstudio.infinitytraffic.initApi.services;

import com.tdstudio.infinitytraffic.initApi.body.DirectionBody;
import com.tdstudio.infinitytraffic.initApi.response.BaseResponse;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("place/autocomplete/json")
    Call<PredictionsResponse> getPredictionPlaces(@Query("input") String input,
                                                  @Query("location") String location,
                                                  @Query("key") String key);

    @GET("apiNodes/{hour}")
    Call<JamTrafficResponse> getJamTraffic(@Path("hour") int hour);

    @POST("apiStressOpt")
    Call<BaseResponse> postDirections(@Body DirectionBody body);
}