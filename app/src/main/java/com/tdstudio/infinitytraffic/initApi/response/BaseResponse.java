package com.tdstudio.infinitytraffic.initApi.response;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * BaseResponse
 *
 * @author QuyDP
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseResponse {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;
    @SerializedName("error")
    private int error;
}
