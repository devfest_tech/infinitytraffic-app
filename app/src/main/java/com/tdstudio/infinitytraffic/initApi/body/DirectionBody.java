package com.tdstudio.infinitytraffic.initApi.body;

import com.google.android.gms.maps.model.LatLng;
import com.tdstudio.infinitytraffic.model.LatLngModel;

import lombok.Data;

@Data
public class DirectionBody {
    private LatLngModel placeA;
    private LatLngModel placeB;
}
