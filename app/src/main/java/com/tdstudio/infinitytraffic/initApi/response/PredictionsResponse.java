package com.tdstudio.infinitytraffic.initApi.response;

import com.tdstudio.infinitytraffic.model.autocompletePlace.Prediction;

import java.util.List;

import lombok.Data;

@Data
public class PredictionsResponse extends BaseResponse{
    private List<Prediction> predictions;
}
