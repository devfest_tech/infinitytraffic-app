package com.tdstudio.infinitytraffic.initApi.body;

import lombok.Data;

@Data
public class JamTrafficBody {
    int hour;
}
