package com.tdstudio.infinitytraffic.initApi.core;

import android.util.Log;

import com.google.gson.Gson;
import com.tdstudio.infinitytraffic.enums.ErrorServerType;
import com.tdstudio.infinitytraffic.initApi.response.BaseResponse;

import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by bapvn on 08/08/2018.
 */

public abstract class ApiCallback<T extends BaseResponse> implements Callback<T> {
    private static final String TAG = ApiCallback.class.getSimpleName();

    public abstract void onSuccess(T response);

    public abstract void onFailed(ApiError apiError);

    private static OnServerResponseListener sListener;

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.code() == 200) {
            Log.d(TAG, "onResponse: " + response.body());

            if (response.body() != null) {
                onSuccess(response.body());
            } else {
                Reader reader = new InputStreamReader(response.errorBody().byteStream(), Charset.forName("UTF-8"));
                Gson gson = new Gson();
                ApiError apiError = gson.fromJson(reader, ApiError.class);
                handlerError(call, apiError);
            }
        } else if (response.code() >= 400 && response.code() < 500) {
            Reader reader = new InputStreamReader(response.errorBody().byteStream(), Charset.forName("UTF-8"));
            Gson gson = new Gson();
            try {
                ApiError apiError = gson.fromJson(reader, ApiError.class);
                handlerError(call, new ApiError(apiError.getError(), apiError.getMessage()));
            } catch (Exception e) {
                e.printStackTrace();
                handlerError(call, new ApiError(response.code(), "Error!"));
            }
        } else {
            handlerError(call, new ApiError(response.code(), response.message()));
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        ApiError error = new ApiError(0, t.getMessage());
        handlerError(call, error);
    }

    private void handlerError(Call<T> call, ApiError error) {
        // TODO: 08/08/2018 handle common function
        if (error.getCode() == ErrorServerType.INVALID_TOKEN.getCode() || error.getCode() == ErrorServerType.NOT_AUTHENTICATION.getCode()) {
            sListener.invalidToken();
        } else if (error.getCode() == ErrorServerType.FORCE_UPDATE.getCode()) {
            sListener.forceUpdate();
        } else if (error.getCode() == ErrorServerType.HAVE_BLOCKED.getCode()) {
            sListener.haveBeenBlock();
        } else {
            onFailed(error);
        }
    }

    public interface OnServerResponseListener {
        void invalidToken();

        void forceUpdate();

        void haveBeenBlock();
    }

    public static void setServerResponseListener(OnServerResponseListener listener) {
        sListener = listener;
    }
}
