package com.tdstudio.infinitytraffic.initApi.core;

import android.content.Context;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class ApiConfig {
    private Context context;
    private String baseUrl;
    private String auth;
    private String token;
}
