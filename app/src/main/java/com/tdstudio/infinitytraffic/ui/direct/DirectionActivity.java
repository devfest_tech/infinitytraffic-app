package com.tdstudio.infinitytraffic.ui.direct;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.tdstudio.infinitytraffic.R;
import com.tdstudio.infinitytraffic.ggmap.DirectionFinder;
import com.tdstudio.infinitytraffic.ggmap.Route;
import com.tdstudio.infinitytraffic.ui.search.SearchActivity;
import com.tdstudio.infinitytraffic.utils.MapUtils;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DirectionActivity extends AppCompatActivity implements OnMapReadyCallback,
        DirectionFinder.DirectionFinderListener {

    //BindView
    @BindView(R.id.mTvFrom)
    TextView mTvFrom;
    @BindView(R.id.mTvTo)
    TextView mTvTo;

    private static final int REQUEST_ADDRESS_CODE = 100;
    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;
    private boolean mIsFrom;
    private LatLng mLatLngFrom;
    private LatLng mLatLngTo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_direction);
        ButterKnife.bind(this);
        initMaps();
    }

    private void initMaps() {
        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap == null) {
            return;
        }
        mMap = googleMap;
//        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//        String shopName = mPost.getShop() != null ? mPost.getShop().getName() : "Cửa hàng";
//        MapUtils.getInstance(googleMap).markPlaceOnMap(getContext(), googleMap, shopName, mPost.getShopAddress());
    }

    @OnClick({R.id.mLnFrom, R.id.mLnTo})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.mLnFrom:
                mIsFrom = true;
                Intent intentFrom = new Intent(this, SearchActivity.class);
                intentFrom.putExtra("isFrom", mIsFrom);
                startActivityForResult(intentFrom, REQUEST_ADDRESS_CODE);
                break;

            case R.id.mLnTo:
                mIsFrom = false;
                Intent intentTo = new Intent(this, SearchActivity.class);
                intentTo.putExtra("isFrom", mIsFrom);
                startActivityForResult(intentTo, REQUEST_ADDRESS_CODE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK
                && requestCode == REQUEST_ADDRESS_CODE
                && data != null
                && data.getExtras() != null) {
            String placeId = data.getStringExtra("placeId");
            getPlaceById(placeId);

        }
    }

    private void getPlaceById(String placeId) {

        // Specify the fields to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME,
                Place.Field.LAT_LNG, Place.Field.ADDRESS);

        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.newInstance(placeId, placeFields);
        PlacesClient placesClient = Places.createClient(this);
        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
            setPlaceToMap(place);
            if (mIsFrom) {
                mTvFrom.setText(place.getName());
                mLatLngFrom = place.getLatLng();
            } else {
                mTvTo.setText(place.getName());
                mLatLngTo = place.getLatLng();
            }
            if (mLatLngFrom != null && mLatLngTo != null) {
                mMap.clear();
                setDirectionMap();
            }
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                // Handle error with given status code.
                Log.e("xxx", "Place not found: " + exception.getMessage());
            }
        });
    }

    private void setPlaceToMap(Place place) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 15));
        mMap.addMarker(new MarkerOptions()
                .position(place.getLatLng())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
    }

    private void setDirectionMap() {
        String latLngA = mLatLngFrom.latitude + ", " + mLatLngFrom.longitude;
        String latLngB = mLatLngTo.latitude + ", " + mLatLngTo.longitude;
        MapUtils.getInstance(mMap).findTheRoadToShop(this, latLngA, latLngB);
    }

    @Override
    public void onDirectionFinderStart() {
        MapUtils.getInstance().findRoadStart();
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        MapUtils.getInstance().findRoadSuccess(routes, "Place");
    }
}
