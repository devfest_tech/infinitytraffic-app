package com.tdstudio.infinitytraffic.ui.search;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.tdstudio.infinitytraffic.model.autocompletePlace.Prediction;

public interface OnSelectPlaceListener {
    void onSelectPlace(AutocompletePrediction prediction);
}
