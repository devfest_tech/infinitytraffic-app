package com.tdstudio.infinitytraffic.ui.jam;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.rilixtech.materialfancybutton.MaterialFancyButton;
import com.tdstudio.infinitytraffic.R;
import com.tdstudio.infinitytraffic.initApi.body.JamTrafficBody;
import com.tdstudio.infinitytraffic.initApi.core.ApiClient;
import com.tdstudio.infinitytraffic.initApi.core.ApiConfig;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;
import com.tdstudio.infinitytraffic.model.JamModel;
import com.tdstudio.infinitytraffic.model.JamTraffic;
import com.tdstudio.infinitytraffic.model.LatLngModel;
import com.tdstudio.infinitytraffic.presenter.jamTraffic.JamTrafficPresenter;
import com.tdstudio.infinitytraffic.presenter.jamTraffic.JamTrafficView;
import com.tdstudio.infinitytraffic.ui.direct.DirectionActivity;
import com.tdstudio.infinitytraffic.ui.search.SearchActivity;
import com.tdstudio.infinitytraffic.utils.CustomInfoWindowAdapter;
import com.tdstudio.infinitytraffic.utils.MapRipple;
import com.tdstudio.infinitytraffic.utils.PermissionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMyLocationClickListener,
        GoogleMap.OnMyLocationButtonClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback{
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int SEARCH_PLACE_CODE = 100;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    @BindView(R.id.mFaBTime)
    FloatingActionButton mFaBTime;
    @BindView(R.id.mRlNow)
    RelativeLayout mRlNow;
    @BindView(R.id.mRlOneHour)
    RelativeLayout mRlOneHour;
    @BindView(R.id.mRlTwoHours)
    RelativeLayout mRlTwoHours;
    @BindView(R.id.mRlThreeHours)
    RelativeLayout mRlThreeHours;
    @BindView(R.id.mRlFourHours)
    RelativeLayout mRlFourHours;
    @BindView(R.id.mRlPlaceDetail)
    RelativeLayout mRlPlaceDetail;
    @BindView(R.id.mTvPrimary)
    TextView mTvPrimary;
    @BindView(R.id.mTvSecond)
    TextView mTvSecond;
    @BindView(R.id.mLnDirection)
    LinearLayout mLnDirection;
    @BindView(R.id.mFaBLocation)
    FloatingActionButton mFaBLocation;
    @BindView(R.id.mLnLocation)
    LinearLayout mLnLocation;
    @BindView(R.id.mBtnNow)
    MaterialFancyButton mBtnNow;
    @BindView(R.id.mTvNow)
    TextView mTvNow;
    @BindView(R.id.mBtnOneHour)
    MaterialFancyButton mBtnOneHour;
    @BindView(R.id.mTvOneHours)
    TextView mTvOneHours;
    @BindView(R.id.mBtnTwoHours)
    MaterialFancyButton mBtnTwoHours;
    @BindView(R.id.mTvTwoHours)
    TextView mTvTwoHours;
    @BindView(R.id.mBtnThreeHours)
    MaterialFancyButton mBtnThreeHours;
    @BindView(R.id.mTvThreeHours)
    TextView mTvThreeHours;
    @BindView(R.id.mBtnFourHours)
    MaterialFancyButton mBtnFourHours;
    @BindView(R.id.mTvFourHours)
    TextView mTvFourHours;

    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private boolean mLocationPermissionGranted = true;
    private Location mLastKnownLocation;
    private LatLng mDefaultLocation = new LatLng(0, 0);

    private boolean mIsShowAction;
    private JamTraffic mCurrentTraffic = new JamTraffic();
    private JamTraffic mAfterOneHour = new JamTraffic();
    private JamTraffic mAfterTwoHours = new JamTraffic();
    private JamTraffic mAfterThreeHours = new JamTraffic();
    private JamTraffic mAfterFourHours = new JamTraffic();

    private List<MapRipple> mMapRipples = new ArrayList<>();

    private int timeSelect = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        initPlaces();
        initRetrofit();
        requestJamTraffic();
    }

    private void requestJamTraffic() {
        JamTrafficBody body = new JamTrafficBody();
        body.setHour(4);
        new JamTrafficPresenter(new JamTrafficView() {
            @Override
            public void onError(String message, int code) {
                Log.e(TAG, "onError: " + message);
            }

            @Override
            public void onSuccess(JamTrafficResponse result) {
                Log.d(TAG, "onSuccess: " + result);
            }
        }).request(body);
    }


    private void fakeCurrent() {
        JamModel jam1 = new JamModel(new LatLngModel(16.075520, 108.223541), 1, "Light");
        JamModel jam2 = new JamModel(new LatLngModel(16.071829, 108.223878), 2, "Quite Light");
        JamModel jam3 = new JamModel(new LatLngModel(16.075118, 108.221582), 3, "Heavy");
        JamModel jam4 = new JamModel(new LatLngModel(16.071456, 108.221118), 4, "Very Heavy");
        List<JamModel> jamModels = new ArrayList<>();
        jamModels.add(jam1);
        jamModels.add(jam2);
        jamModels.add(jam3);
        jamModels.add(jam4);
        mCurrentTraffic = new JamTraffic();
        mCurrentTraffic.setId(0);
        mCurrentTraffic.setJamModels(jamModels);

        // Add lots of markers to the map.
        addMarkersToMap(mCurrentTraffic);

        //Ripple
        initializeRipple(mCurrentTraffic);
    }

    private void fakeAfterOneHours() {
        JamModel jam1 = new JamModel(new LatLngModel(16.075520, 108.223541), 4, "Light");
        JamModel jam2 = new JamModel(new LatLngModel(16.071829, 108.223878), 3, "Quite Light");
        JamModel jam3 = new JamModel(new LatLngModel(16.075118, 108.221582), 2, "Heavy");
        JamModel jam4 = new JamModel(new LatLngModel(16.071456, 108.221118), 1, "Very Heavy");
        List<JamModel> jamModels = new ArrayList<>();
        jamModels.add(jam1);
        jamModels.add(jam2);
        jamModels.add(jam3);
        jamModels.add(jam4);
        mAfterOneHour.setId(0);
        mAfterOneHour.setJamModels(jamModels);

        // Add lots of markers to the map.
        addMarkersToMap(mAfterOneHour);

        //Ripple
        initializeRipple(mAfterOneHour);
    }

    private void fakeAfterTwoHours() {
        JamModel jam1 = new JamModel(new LatLngModel(16.075520, 108.223541), 3, "Light");
        JamModel jam2 = new JamModel(new LatLngModel(16.071829, 108.223878), 1, "Quite Light");
        JamModel jam3 = new JamModel(new LatLngModel(16.075118, 108.221582), 4, "Heavy");
        JamModel jam4 = new JamModel(new LatLngModel(16.071456, 108.221118), 2, "Very Heavy");
        List<JamModel> jamModels = new ArrayList<>();
        jamModels.add(jam1);
        jamModels.add(jam2);
        jamModels.add(jam3);
        jamModels.add(jam4);
        mAfterTwoHours.setId(0);
        mAfterTwoHours.setJamModels(jamModels);

        // Add lots of markers to the map.
        addMarkersToMap(mAfterTwoHours);

        //Ripple
        initializeRipple(mAfterTwoHours);
    }


    private void fakeAfterThreeHours() {
        JamModel jam1 = new JamModel(new LatLngModel(16.075520, 108.223541), 4, "Light");
        JamModel jam2 = new JamModel(new LatLngModel(16.071829, 108.223878), 2, "Quite Light");
        JamModel jam3 = new JamModel(new LatLngModel(16.075118, 108.221582), 3, "Heavy");
        JamModel jam4 = new JamModel(new LatLngModel(16.071456, 108.221118), 1, "Very Heavy");
        List<JamModel> jamModels = new ArrayList<>();
        jamModels.add(jam1);
        jamModels.add(jam2);
        jamModels.add(jam3);
        jamModels.add(jam4);
        mAfterThreeHours.setId(0);
        mAfterThreeHours.setJamModels(jamModels);

        // Add lots of markers to the map.
        addMarkersToMap(mAfterThreeHours);

        //Ripple
        initializeRipple(mAfterThreeHours);
    }

    private void fakeAfterFourHours() {
        JamModel jam1 = new JamModel(new LatLngModel(16.075520, 108.223541), 3, "Light");
        JamModel jam2 = new JamModel(new LatLngModel(16.071829, 108.223878), 2, "Quite Light");
        JamModel jam3 = new JamModel(new LatLngModel(16.075118, 108.221582), 1, "Heavy");
        JamModel jam4 = new JamModel(new LatLngModel(16.071456, 108.221118), 4, "Very Heavy");
        List<JamModel> jamModels = new ArrayList<>();
        jamModels.add(jam1);
        jamModels.add(jam2);
        jamModels.add(jam3);
        jamModels.add(jam4);
        mAfterFourHours.setId(0);
        mAfterFourHours.setJamModels(jamModels);

        // Add lots of markers to the map.
        addMarkersToMap(mAfterFourHours);

        //Ripple
        initializeRipple(mAfterFourHours);
    }

    private void initPlaces() {
        String apiKey = getString(R.string.map_apis_key);
        // Setup Places Client
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }
    }


    @OnClick({R.id.mLnSearchBar, R.id.mFaBLocation, R.id.mLnDirection, R.id.mFaBTime,
            R.id.mBtnOneHour, R.id.mBtnTwoHours, R.id.mBtnThreeHours, R.id.mBtnFourHours})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.mLnSearchBar:
                startActivityForResult(new Intent(this, SearchActivity.class), SEARCH_PLACE_CODE);
                break;
            case R.id.mFaBLocation:
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLastKnownLocation.getLatitude(),
                                mLastKnownLocation.getLongitude()), 15));
                break;
            case R.id.mLnDirection:
                startActivity(new Intent(this, DirectionActivity.class));
                break;

            case R.id.mFaBTime:
                if (mIsShowAction) {
                    mIsShowAction = false;
                    Animation animate = AnimationUtils.loadAnimation(this, R.anim.rotate_backward);
                    mFaBTime.startAnimation(animate);
                    hideOptionView(4);
                } else {
                    mIsShowAction = true;
                    Animation animate = AnimationUtils.loadAnimation(this, R.anim.rotate_foreward);
                    mFaBTime.startAnimation(animate);
                    showOptionView(0);
                }
                break;
            case R.id.mBtnNow:
                if(timeSelect == 0){
                    return;
                }
                mBtnNow.setBackgroundColor(getColor(R.color.colorPrimary));
                mTvNow.setTextColor(getColor(R.color.colorWhite));

                mBtnOneHour.setBackgroundColor(getColor(R.color.colorWhite));
                mTvOneHours.setTextColor(getColor(R.color.colorBlack));

                mBtnTwoHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvTwoHours.setTextColor(getColor(R.color.colorBlack));

                mBtnThreeHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvThreeHours.setTextColor(getColor(R.color.colorBlack));

                mBtnFourHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvFourHours.setTextColor(getColor(R.color.colorBlack));

                fakeCurrent();
                break;

            case R.id.mBtnOneHour:
                if(timeSelect == 1){
                    return;
                }
                mBtnNow.setBackgroundColor(getColor(R.color.colorWhite));
                mTvNow.setTextColor(getColor(R.color.colorBlack));

                mBtnOneHour.setBackgroundColor(getColor(R.color.colorPrimary));
                mTvOneHours.setTextColor(getColor(R.color.colorWhite));

                mBtnTwoHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvTwoHours.setTextColor(getColor(R.color.colorBlack));

                mBtnThreeHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvThreeHours.setTextColor(getColor(R.color.colorBlack));

                mBtnFourHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvFourHours.setTextColor(getColor(R.color.colorBlack));

                fakeAfterOneHours();
                break;
            case R.id.mBtnTwoHours:
                if(timeSelect == 2){
                    return;
                }
                mBtnNow.setBackgroundColor(getColor(R.color.colorWhite));
                mTvNow.setTextColor(getColor(R.color.colorBlack));

                mBtnOneHour.setBackgroundColor(getColor(R.color.colorWhite));
                mTvOneHours.setTextColor(getColor(R.color.colorBlack));

                mBtnTwoHours.setBackgroundColor(getColor(R.color.colorPrimary));
                mTvTwoHours.setTextColor(getColor(R.color.colorWhite));

                mBtnThreeHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvThreeHours.setTextColor(getColor(R.color.colorBlack));

                mBtnFourHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvFourHours.setTextColor(getColor(R.color.colorBlack));

                fakeAfterTwoHours();

                break;
            case R.id.mBtnThreeHours:
                if(timeSelect == 3){
                    return;
                }
                mBtnNow.setBackgroundColor(getColor(R.color.colorWhite));
                mTvNow.setTextColor(getColor(R.color.colorBlack));

                mBtnOneHour.setBackgroundColor(getColor(R.color.colorWhite));
                mTvOneHours.setTextColor(getColor(R.color.colorBlack));

                mBtnTwoHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvTwoHours.setTextColor(getColor(R.color.colorBlack));

                mBtnThreeHours.setBackgroundColor(getColor(R.color.colorPrimary));
                mTvThreeHours.setTextColor(getColor(R.color.colorWhite));

                mBtnFourHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvFourHours.setTextColor(getColor(R.color.colorBlack));
                fakeAfterThreeHours();

                break;
            case R.id.mBtnFourHours:
                if(timeSelect == 4){
                    return;
                }
                mBtnNow.setBackgroundColor(getColor(R.color.colorWhite));
                mTvNow.setTextColor(getColor(R.color.colorBlack));

                mBtnOneHour.setBackgroundColor(getColor(R.color.colorWhite));
                mTvOneHours.setTextColor(getColor(R.color.colorBlack));

                mBtnTwoHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvTwoHours.setTextColor(getColor(R.color.colorBlack));

                mBtnThreeHours.setBackgroundColor(getColor(R.color.colorWhite));
                mTvThreeHours.setTextColor(getColor(R.color.colorBlack));

                mBtnFourHours.setBackgroundColor(getColor(R.color.colorPrimary));
                mTvFourHours.setTextColor(getColor(R.color.colorWhite));
                fakeAfterFourHours();
                break;
        }
    }

    /**
     * Execute animation when user hide more option.
     *
     * @param index
     */
    private void hideOptionView(int index) {
        if (index >= 0) {
            switch (index) {
                case 0:
                    hideView(mRlNow, index);
                    break;

                case 1:
                    hideView(mRlOneHour, index);
                    break;

                case 2:
                    hideView(mRlTwoHours, index);
                    break;

                case 3:
                    hideView(mRlThreeHours, index);
                    break;

                case 4:
                    hideView(mRlFourHours, index);
                    break;
            }
        }
    }

    private void showOptionView(int index) {
        if (index <= 4) {
            switch (index) {
                case 0:
                    showView(mRlNow, index);
                    break;

                case 1:
                    showView(mRlOneHour, index);
                    break;

                case 2:
                    showView(mRlTwoHours, index);
                    break;

                case 3:
                    showView(mRlThreeHours, index);
                    break;

                case 4:
                    showView(mRlFourHours, index);
                    break;

            }
        }
    }

    private void hideView(View view, int index) {
        Animation mAnimationClose = AnimationUtils.loadAnimation(this, R.anim.fab_close);
        view.startAnimation(mAnimationClose);
        mAnimationClose.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
                hideOptionView(index - 1);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void showView(View view, int index) {
        Animation mAnimationOpen = AnimationUtils.loadAnimation(this, R.anim.fab_open);
        view.setVisibility(View.VISIBLE);
        view.startAnimation(mAnimationOpen);
        mAnimationOpen.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                showOptionView(index + 1);
                view.setClickable(true);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SEARCH_PLACE_CODE && resultCode == RESULT_OK && data != null) {
            String placeId = data.getStringExtra("placeId");
            getPlaceById(placeId);
        }
    }

    private void getPlaceById(String placeId) {

        // Specify the fields to return.
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME,
                Place.Field.LAT_LNG, Place.Field.ADDRESS);

        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.newInstance(placeId, placeFields);
        PlacesClient placesClient = Places.createClient(this);
        placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
            Place place = response.getPlace();
            setPlaceToMap(place);
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                int statusCode = apiException.getStatusCode();
                // Handle error with given status code.
                Log.e(TAG, "Place not found: " + exception.getMessage());
            }
        });
    }

    private void setPlaceToMap(Place place) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.getLatLng(), 18));
        mMap.addMarker(new MarkerOptions()
                .position(place.getLatLng())
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        mTvPrimary.setText(place.getName());
        mTvSecond.setText(place.getAddress());
        slideDown(mLnDirection);
        slideDown(mLnLocation);
        slideUp(mRlPlaceDetail);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMyLocationButtonClickListener(this);
        mMap.setOnMyLocationClickListener(this);
        //Set position of my location button
        mMap.setPadding(0, getResources().getDimensionPixelOffset(R.dimen._270sdp), 0, 0);


        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        getDeviceLocation();
//        enableMyLocation();

        //Jam Traffic current
        fakeCurrent();

        // Setting an info window adapter allows us to change the both the contents and look of the
        // info window.
        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(this, "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull Location location) {
        Toast.makeText(this, "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    private void addMarkersToMap(JamTraffic currentTraffic) {
        mMap.clear();
        for (JamModel jamModel : currentTraffic.getJamModels()) {
            MarkerOptions markerOptions = new MarkerOptions();
            switch (jamModel.getStatus()) {
                case 1:
                    markerOptions = new MarkerOptions()
                            .position(new LatLng(jamModel.getPosition().getLat(), jamModel.getPosition().getLng()))
                            .title("Place A")
                            .snippet("Tình trạng: Thông ")
                            .snippet("Mô tả: Thông thoáng, không kẹt xe")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
                    break;
                case 2:
                    markerOptions = new MarkerOptions()
                            .position(new LatLng(jamModel.getPosition().getLat(), jamModel.getPosition().getLng()))
                            .title("Place A")
                            .snippet("Tình trạng: Bình Th ")
                            .snippet("Mô tả: Thông thoáng, không kẹt xe")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW));
                    break;
                case 3:
                    markerOptions = new MarkerOptions()
                            .position(new LatLng(jamModel.getPosition().getLat(), jamModel.getPosition().getLng()))
                            .title("Place A")
                            .snippet("Tình trạng: Kha Nang")
                            .snippet("Mô tả: Thông thoáng, không kẹt xe")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));
                    break;
                case 4:
                    markerOptions = new MarkerOptions()
                            .position(new LatLng(jamModel.getPosition().getLat(), jamModel.getPosition().getLng()))
                            .title("Place A")
                            .snippet("Tình trạng: Tac Nghen")
                            .snippet("Mô tả: Thông thoáng, không kẹt xe")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    break;
            }
            mMap.addMarker(markerOptions);
        }

    }


    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            // Access to the location has been granted to the app.
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }


    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = (Location) task.getResult();
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                    new LatLng(mLastKnownLocation.getLatitude(),
                                            mLastKnownLocation.getLongitude()), 15));
                            mMap.getUiSettings().setMyLocationButtonEnabled(true);
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, 30));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
        }
    }

    private void initializeRipple(JamTraffic jamTraffic) {
        for (MapRipple ripple : mMapRipples) {
            ripple.stopRippleMapAnimation();
        }
        mMapRipples.clear();
        for (JamModel jam : jamTraffic.getJamModels()) {
            LatLng latLng = new LatLng(jam.getPosition().getLat(), jam.getPosition().getLng());
            MapRipple mapRipple = new MapRipple(mMap, latLng, this);
            switch (jam.getStatus()) {
                case 2:
                    mapRipple.withFillColor(Color.parseColor("#FFC425"));
                    break;
                case 3:
                    mapRipple.withFillColor(Color.parseColor("#F37735"));
                    break;
                case 4:
                    mapRipple.withFillColor(Color.parseColor("#D11141"));
                    break;
                default:
                    mapRipple.withFillColor(Color.parseColor("#00B159"));
                    break;
            }
            mapRipple
                    .withStrokeWidth(0)
                    .startRippleMapAnimation();
            mMapRipples.add(mapRipple);
        }
    }

    private void initRetrofit() {
        String baseUrl = "http://22cc3d1f.ngrok.io";
        //api backend
        ApiConfig apiConfigApiBackend = ApiConfig.builder()
                .baseUrl(baseUrl)
                .context(getApplicationContext())
                .build();
        ApiClient.getInstance().init(apiConfigApiBackend);
    }

    /**
     * slide the view from below itself to the current position
     *
     * @param view
     */
    public void slideUp(View view) {
        view.setVisibility(View.VISIBLE);
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                view.getHeight(),  // fromYDelta
                0);                // toYDelta
        animate.setDuration(400);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animate);
    }

    /**
     * slide the view from its current position to below itself
     *
     * @param view
     */
    public void slideDown(final View view) {
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(300);
        animate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        view.startAnimation(animate);
    }


}
