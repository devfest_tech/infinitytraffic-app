package com.tdstudio.infinitytraffic.ui.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.tdstudio.infinitytraffic.R;
import com.tdstudio.infinitytraffic.base.BaseAdapter;

import java.util.List;

public class SearchPlaceAdapter extends BaseAdapter<SearchPlaceAdapter.PredictionPlaceHolder> {
    private List<AutocompletePrediction> mListPrediction;
    private OnSelectPlaceListener mListener;
    protected SearchPlaceAdapter(@NonNull Context context, List<AutocompletePrediction> predictionList,
                                 OnSelectPlaceListener listener) {
        super(context);
        mListPrediction = predictionList;
        mListener = listener;
    }

    @NonNull
    @Override
    public PredictionPlaceHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(getContext()).inflate(R.layout.item_search_place, parent, false);
        return new PredictionPlaceHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PredictionPlaceHolder holder, int position) {
        holder.mTvPrimary.setText(mListPrediction.get(position).getPrimaryText(null).toString());
        holder.mTvSecond.setText(mListPrediction.get(position).getSecondaryText(null).toString());
    }

    @Override
    public int getItemCount() {
        return mListPrediction.size()   ;
    }

    class PredictionPlaceHolder extends RecyclerView.ViewHolder{
        TextView mTvPrimary;
        TextView mTvSecond;

        public PredictionPlaceHolder(@NonNull View itemView) {
            super(itemView);
            mTvPrimary = itemView.findViewById(R.id.mTvPrimary);
            mTvSecond = itemView.findViewById(R.id.mTvSecond);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onSelectPlace(mListPrediction.get(getLayoutPosition()));
                }
            });
        }
    }


}
