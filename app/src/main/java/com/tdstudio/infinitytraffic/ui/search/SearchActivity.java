package com.tdstudio.infinitytraffic.ui.search;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.RectangularBounds;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.tdstudio.infinitytraffic.R;
import com.tdstudio.infinitytraffic.initApi.core.ApiClient;
import com.tdstudio.infinitytraffic.initApi.core.ApiConfig;
import com.tdstudio.infinitytraffic.utils.RecyclerViewUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity {
    @BindView(R.id.mRecyclerPlace)
    RecyclerView mRecycleSearchHere;
    @BindView(R.id.mEdtSearch)
    EditText mEdtSearchHere;
    @BindView(R.id.mImgBack)
    ImageView mImgBack;

    private static final String TAG = SearchActivity.class.getSimpleName();
    private List<AutocompletePrediction> mListPrediction = new ArrayList<>();
    private SearchPlaceAdapter mAdapter;
    private String searchKey = "";
    private PlacesClient placesClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        overridePendingTransition(R.anim.slide_up_info, R.anim.no_change);
        placesClient = Places.createClient(this);
        initRetrofit();
        initAdapter();
        listenSearchPlace();
    }

    private void listenSearchPlace() {
        mEdtSearchHere.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                searchKey = s.toString().trim();
                autocompletePlace();
            }
        });
    }

    private void initAdapter() {
        mAdapter = new SearchPlaceAdapter(this, mListPrediction, new OnSelectPlaceListener() {
            @Override
            public void onSelectPlace(AutocompletePrediction prediction) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("placeId", prediction.getPlaceId());
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });
        RecyclerViewUtils.Create().setUpVertical(this, mRecycleSearchHere);
        mRecycleSearchHere.setAdapter(mAdapter);
    }

    private void initRetrofit() {
        String baseUrl = "https://maps.googleapis.com/maps/api/";
        //api backend
        ApiConfig apiConfigApiBackend = ApiConfig.builder()
                .baseUrl(baseUrl)
                .context(getApplicationContext())
                .build();
        ApiClient.getInstance().init(apiConfigApiBackend);
    }

    private void autocompletePlace() {
        // Create a new token for the autocomplete session. Pass this to FindAutocompletePredictionsRequest,
        // and once again when the user makes a selection (for example when calling fetchPlace()).
        AutocompleteSessionToken token = AutocompleteSessionToken.newInstance();

        // Create a RectangularBounds object.
        RectangularBounds bounds = RectangularBounds.newInstance(
                new LatLng(15.999928, 108.190162), // southwest
                new LatLng(16.096146, 108.214981)); // northeast
        // Use the builder to create a FindAutocompletePredictionsRequest.
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                // Call either setLocationBias() OR setLocationRestriction().
                .setLocationBias(bounds)
                //.setLocationRestriction(bounds)
                .setCountry("vn")
                .setTypeFilter(TypeFilter.ADDRESS)
                .setSessionToken(token)
                .setQuery(searchKey)
                .build();

        placesClient.findAutocompletePredictions(request).addOnSuccessListener((response) -> {
            mListPrediction.clear();
            mListPrediction.addAll(response.getAutocompletePredictions());
            mAdapter.notifyDataSetChanged();
            for (AutocompletePrediction prediction : response.getAutocompletePredictions()) {
                Log.i(TAG, prediction.getPlaceId());
                Log.i(TAG, prediction.getPrimaryText(null).toString());
            }
        }).addOnFailureListener((exception) -> {
            if (exception instanceof ApiException) {
                ApiException apiException = (ApiException) exception;
                Log.e(TAG, "Place not found: " + apiException.getStatusCode());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.no_change, R.anim.slide_down_info);
    }

    @OnClick({R.id.mImgBack})
    void onClick(View view){
        switch (view.getId()){
            case R.id.mImgBack:
                onBackPressed();
                break;
        }
    }
}
