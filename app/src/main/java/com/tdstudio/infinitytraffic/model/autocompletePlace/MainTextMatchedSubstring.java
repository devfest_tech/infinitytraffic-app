package com.tdstudio.infinitytraffic.model.autocompletePlace;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MainTextMatchedSubstring {
    @SerializedName("length")
    private long length;
    @SerializedName("offset")
    private long offset;
}
