package com.tdstudio.infinitytraffic.model.autocompletePlace;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class MatchedSubstring {
    @SerializedName("length")
    public long length;
    @SerializedName("offset")
    public long offset;
}
