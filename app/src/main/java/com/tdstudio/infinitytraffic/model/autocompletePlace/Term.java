package com.tdstudio.infinitytraffic.model.autocompletePlace;

import com.google.gson.annotations.SerializedName;

public class Term {
    @SerializedName("offset")
    public long offset;
    @SerializedName("value")
    public String value;
}
