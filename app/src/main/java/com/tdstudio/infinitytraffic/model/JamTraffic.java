package com.tdstudio.infinitytraffic.model;

import java.util.List;

import lombok.Data;

@Data
public class JamTraffic {
    private int id;
    private List<JamModel> jamModels;
}
