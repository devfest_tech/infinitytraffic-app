package com.tdstudio.infinitytraffic.model;

import lombok.Data;

@Data
public class LatLngModel {
    private double lat;
    private double lng;

    public LatLngModel(){

    }

    public LatLngModel(double lat, double lng){
        this.lat = lat;
        this.lng = lng;
    }


}
