package com.tdstudio.infinitytraffic.model.autocompletePlace;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class Prediction {
    @SerializedName("description")
    String description;
    @SerializedName("id")
    String id;
    @SerializedName("matched_substrings")
    List<MatchedSubstring> matchedSubstrings = null;
    @SerializedName("place_id")
    String placeId;
    @SerializedName("reference")
    String reference;
    @SerializedName("structured_formatting")
    StructuredFormatting structuredFormatting;
    @SerializedName("terms")
    List<Term> terms = null;
    @SerializedName("types")
    List<String> types = null;
}
