package com.tdstudio.infinitytraffic.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class JamModel {
    private long id;
    private String name;
    private LatLngModel position;
    @SerializedName("alertLevel")
    private int status;
    @SerializedName("reason")
    private String description;

    public JamModel(LatLngModel position, int status, String description) {
        this.position = position;
        this.status = status;
        this.description = description;
    }
}
