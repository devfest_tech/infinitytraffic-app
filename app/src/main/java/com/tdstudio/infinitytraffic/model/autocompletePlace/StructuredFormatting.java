package com.tdstudio.infinitytraffic.model.autocompletePlace;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class StructuredFormatting {
    @SerializedName("main_text")
    String mainText;
    @SerializedName("main_text_matched_substrings")
    List<MainTextMatchedSubstring> mainTextMatchedSubstrings = null;
    @SerializedName("secondary_text")
    String secondaryText;
}
