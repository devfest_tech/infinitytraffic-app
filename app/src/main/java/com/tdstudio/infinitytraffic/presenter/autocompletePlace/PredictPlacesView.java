package com.tdstudio.infinitytraffic.presenter.autocompletePlace;

import com.tdstudio.infinitytraffic.base.BaseView;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

public interface PredictPlacesView extends BaseView<PredictionsResponse> {
}
