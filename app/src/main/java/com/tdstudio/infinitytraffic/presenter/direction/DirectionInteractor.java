package com.tdstudio.infinitytraffic.presenter.direction;

import com.tdstudio.infinitytraffic.base.BaseInteractor;
import com.tdstudio.infinitytraffic.base.RequestCallBack;
import com.tdstudio.infinitytraffic.initApi.body.DirectionBody;
import com.tdstudio.infinitytraffic.initApi.body.JamTrafficBody;
import com.tdstudio.infinitytraffic.initApi.core.ApiCallback;
import com.tdstudio.infinitytraffic.initApi.core.ApiClient;
import com.tdstudio.infinitytraffic.initApi.core.ApiError;
import com.tdstudio.infinitytraffic.initApi.response.BaseResponse;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;

import retrofit2.Call;

public class DirectionInteractor extends BaseInteractor<DirectionBody, BaseResponse> {
    private Call<BaseResponse> mRq;

    @Override
    public void request(DirectionBody requestData, RequestCallBack<BaseResponse> callback) {
        mRq = ApiClient.getService().postDirections(requestData);
        mRq.enqueue(new ApiCallback<BaseResponse>() {
            @Override
            public void onSuccess(BaseResponse response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailed(ApiError apiError) {
                callback.onError(apiError.getCode(), apiError.getMessage());
            }
        });
    }

    @Override
    public void cancel() {
        if (mRq != null) {
            mRq.cancel();
        }
    }
}
