package com.tdstudio.infinitytraffic.presenter.jamTraffic;

import com.tdstudio.infinitytraffic.base.BaseInteractor;
import com.tdstudio.infinitytraffic.base.RequestCallBack;
import com.tdstudio.infinitytraffic.initApi.body.JamTrafficBody;
import com.tdstudio.infinitytraffic.initApi.body.PredictionPlaceBody;
import com.tdstudio.infinitytraffic.initApi.core.ApiCallback;
import com.tdstudio.infinitytraffic.initApi.core.ApiClient;
import com.tdstudio.infinitytraffic.initApi.core.ApiError;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;
import com.tdstudio.infinitytraffic.model.JamTraffic;

import retrofit2.Call;

public class JamTrafficInteractor extends BaseInteractor<JamTrafficBody, JamTrafficResponse> {
    private Call<JamTrafficResponse> mRq;

    @Override
    public void request(JamTrafficBody requestData, RequestCallBack<JamTrafficResponse> callback) {
        mRq = ApiClient.getService().getJamTraffic(requestData.getHour());
        mRq.enqueue(new ApiCallback<JamTrafficResponse>() {
            @Override
            public void onSuccess(JamTrafficResponse response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailed(ApiError apiError) {
                callback.onError(apiError.getCode(), apiError.getMessage());
            }
        });
    }

    @Override
    public void cancel() {
        if (mRq != null) {
            mRq.cancel();
        }
    }
}
