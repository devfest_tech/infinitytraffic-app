package com.tdstudio.infinitytraffic.presenter.direction;

import com.tdstudio.infinitytraffic.base.BasePresenter;
import com.tdstudio.infinitytraffic.initApi.body.DirectionBody;
import com.tdstudio.infinitytraffic.initApi.body.JamTrafficBody;
import com.tdstudio.infinitytraffic.initApi.response.BaseResponse;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;

public class DirectionPresenter extends BasePresenter<DirectionView,
        DirectionInteractor, DirectionBody, BaseResponse> {

    public DirectionPresenter(DirectionView view) {
        super(view, new DirectionInteractor());
    }

    @Override
    public void onSuccess(BaseResponse response) {
        getView().onSuccess(response);
    }

    @Override
    public void onError(int code, String message) {
        getView().onError(message, code);
    }

    @Override
    public void request(DirectionBody requestData) {
        getInteractor().request(requestData, this);
    }

    @Override
    public void cancel() {
        if (getInteractor() != null) {
            getInteractor().cancel();
        }
    }
}
