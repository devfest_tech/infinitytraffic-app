package com.tdstudio.infinitytraffic.presenter.autocompletePlace;

import android.util.Log;

import com.tdstudio.infinitytraffic.base.BaseInteractor;
import com.tdstudio.infinitytraffic.base.RequestCallBack;
import com.tdstudio.infinitytraffic.initApi.body.PredictionPlaceBody;
import com.tdstudio.infinitytraffic.initApi.core.ApiCallback;
import com.tdstudio.infinitytraffic.initApi.core.ApiClient;
import com.tdstudio.infinitytraffic.initApi.core.ApiError;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

import retrofit2.Call;

public class PredictPlacesInteractor extends BaseInteractor<PredictionPlaceBody, PredictionsResponse> {
    private Call<PredictionsResponse> mRq;

    @Override
    public void request(PredictionPlaceBody requestData, RequestCallBack<PredictionsResponse> callback) {
        mRq = ApiClient.getService().getPredictionPlaces(requestData.getInput(), requestData.getLocation(),
                requestData.getKey());
        mRq.enqueue(new ApiCallback<PredictionsResponse>() {
            @Override
            public void onSuccess(PredictionsResponse response) {
                callback.onSuccess(response);
            }

            @Override
            public void onFailed(ApiError apiError) {
                callback.onError(apiError.getCode(), apiError.getMessage());
            }
        });
    }

    @Override
    public void cancel() {
        if (mRq != null) {
            mRq.cancel();
        }
    }
}
