package com.tdstudio.infinitytraffic.presenter.jamTraffic;

import com.tdstudio.infinitytraffic.base.BaseView;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

public interface JamTrafficView extends BaseView<JamTrafficResponse> {
}
