package com.tdstudio.infinitytraffic.presenter.jamTraffic;

import com.tdstudio.infinitytraffic.base.BasePresenter;
import com.tdstudio.infinitytraffic.initApi.body.JamTrafficBody;
import com.tdstudio.infinitytraffic.initApi.body.PredictionPlaceBody;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

public class JamTrafficPresenter extends BasePresenter<JamTrafficView,
        JamTrafficInteractor, JamTrafficBody, JamTrafficResponse> {

    public JamTrafficPresenter(JamTrafficView view) {
        super(view, new JamTrafficInteractor());
    }

    @Override
    public void onSuccess(JamTrafficResponse response) {
        getView().onSuccess(response);
    }

    @Override
    public void onError(int code, String message) {
        getView().onError(message, code);
    }

    @Override
    public void request(JamTrafficBody requestData) {
        getInteractor().request(requestData, this);
    }

    @Override
    public void cancel() {
        if (getInteractor() != null) {
            getInteractor().cancel();
        }
    }
}
