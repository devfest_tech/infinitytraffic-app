package com.tdstudio.infinitytraffic.presenter.autocompletePlace;

import com.tdstudio.infinitytraffic.base.BasePresenter;
import com.tdstudio.infinitytraffic.initApi.body.PredictionPlaceBody;
import com.tdstudio.infinitytraffic.initApi.response.PredictionsResponse;

public class PredictPlacesPresenter extends BasePresenter<PredictPlacesView,
        PredictPlacesInteractor, PredictionPlaceBody, PredictionsResponse> {

    public PredictPlacesPresenter(PredictPlacesView view) {
        super(view, new PredictPlacesInteractor());
    }

    @Override
    public void onSuccess(PredictionsResponse response) {
        getView().onSuccess(response);
    }

    @Override
    public void onError(int code, String message) {
        getView().onError(message, code);
    }

    @Override
    public void request(PredictionPlaceBody requestData) {
        getInteractor().request(requestData, this);
    }

    @Override
    public void cancel() {
        if (getInteractor() != null) {
            getInteractor().cancel();
        }
    }
}
