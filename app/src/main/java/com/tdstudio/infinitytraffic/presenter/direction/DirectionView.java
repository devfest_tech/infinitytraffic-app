package com.tdstudio.infinitytraffic.presenter.direction;

import com.tdstudio.infinitytraffic.base.BaseView;
import com.tdstudio.infinitytraffic.initApi.response.BaseResponse;
import com.tdstudio.infinitytraffic.initApi.response.JamTrafficResponse;

public interface DirectionView extends BaseView<BaseResponse> {

}
