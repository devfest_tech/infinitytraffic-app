package com.tdstudio.infinitytraffic.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.tdstudio.infinitytraffic.R;
import com.tdstudio.infinitytraffic.ggmap.DirectionFinder;
import com.tdstudio.infinitytraffic.ggmap.Route;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TuDLT on 5/13/2018.
 */
public class MapUtils {
    private static MapUtils sMapUtils;
    private DirectionFinder mDirectionFinder;
    private GoogleMap mGoogleMap;
    private List<Marker> mOriginMarkers;
    private List<Marker> mDestinationMarkers;
    private List<Polyline> mPolyLinePaths;

    public static MapUtils getInstance(GoogleMap googleMap) {
        if (sMapUtils == null) {
            sMapUtils = new MapUtils();
            sMapUtils.setGoogleMap(googleMap);
        }
        return sMapUtils;
    }

    public static MapUtils getInstance() {
        if (sMapUtils == null) {
            try {
                throw new Exception("The newInstance() function have to call after newInstance(Context context) function.");
            } catch (Exception var1) {
                var1.printStackTrace();
            }
        }

        return sMapUtils;
    }


    private void setGoogleMap(GoogleMap googleMap) {
        mGoogleMap = googleMap;
    }

    @SuppressLint("MissingPermission")
    public String getMyLocation(Context context) {
        mGoogleMap.setMyLocationEnabled(true);
        LocationManager locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        Location location = locationManager.getLastKnownLocation(locationManager
                .getBestProvider(criteria, false));
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        Geocoder geocoder = new Geocoder(context);
        Address address = null;
        List<Address> list;
        try {
            list = geocoder.getFromLocation(myLocation.latitude, myLocation.longitude, 1);
            address = list.get(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return address == null ? null : address.getAddressLine(0);
    }

    public void markPlaceOnMap(Context context, GoogleMap googleMap, String shopName, String addressName) {
        Geocoder geocoder = new Geocoder(context);
        List<Address> list = new ArrayList<>();
        try {
            list = geocoder.getFromLocationName(addressName, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (list.size() > 0) {
            Address address = list.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(latLng).title(shopName).snippet(addressName);
            Marker marker = googleMap.addMarker(markerOptions);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            marker.showInfoWindow();

        }
    }

    public void findTheRoadToShop(DirectionFinder.DirectionFinderListener finderListener, String myLocation, String shopLocation) {
        mDirectionFinder = new DirectionFinder(finderListener, myLocation, shopLocation);
        try {
            mDirectionFinder.execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void findRoadSuccess(List<Route> routes, String shopName) {
        mPolyLinePaths = new ArrayList<>();
        mOriginMarkers = new ArrayList<>();
        mDestinationMarkers = new ArrayList<>();
        for (int index = routes.size() - 1; index >= 0; index--) {
            Route route = routes.get(index);
            mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 15));
            mOriginMarkers.add(mGoogleMap.addMarker(new MarkerOptions().position(route.startLocation)));
            mDestinationMarkers.add(mGoogleMap.addMarker(new MarkerOptions()
                    .position(route.endLocation)));
            PolylineOptions polylineOptions = new PolylineOptions().
                    geodesic(true)
                    .width(15);
            if (index == 0) {
                polylineOptions.color(Color.BLUE);
            } else {
                polylineOptions.color(Color.GRAY);
            }
            for (int i = 0; i < route.points.size(); i++) {
                polylineOptions.add(route.points.get(i));
            }
            mPolyLinePaths.add(mGoogleMap.addPolyline(polylineOptions));
        }
    }

    public void findRoadStart() {
        if (mOriginMarkers != null) {
            for (Marker marker : mOriginMarkers) {
                marker.remove();
            }
        }

        if (mDestinationMarkers != null) {
            for (Marker marker : mDestinationMarkers) {
                marker.remove();
            }
        }

        if (mPolyLinePaths != null) {
            for (Polyline polyline : mPolyLinePaths) {
                polyline.remove();
            }
        }
    }
}
