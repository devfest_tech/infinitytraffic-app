package com.tdstudio.infinitytraffic.enums;

import java.util.HashMap;
import java.util.Map;

public enum DeepLinkType {
    VIDEO("videos"),
    LIVE("livestreams"),
    UNKNOW("UNKNOW");
    private final String mText;

    DeepLinkType(String text) {
        mText = text;
    }

    public String getText() {
        return mText;
    }

    private static final Map<String, DeepLinkType> strToTypeMap = new HashMap<String, DeepLinkType>();

    static {
        for (DeepLinkType type : DeepLinkType.values()) {
            strToTypeMap.put(type.mText, type);
        }
    }

    public static DeepLinkType fromText(String text) {
        DeepLinkType type = strToTypeMap.get(text);
        if (type == null) {
            return UNKNOW;
        }
        return type;
    }
}
