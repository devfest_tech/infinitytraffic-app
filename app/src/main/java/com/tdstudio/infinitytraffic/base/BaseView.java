package com.tdstudio.infinitytraffic.base;

/**
 * Created by bapvn on 08/08/2018.
 */

public interface BaseView<R> {
    void onError(String message, int code);

    void onSuccess(R result);
}
