package com.tdstudio.infinitytraffic.base;

/**
 * Created by bapvn on 08/08/2018.
 */

public abstract class BaseInteractor<T, ObjectData> {
    public abstract void request(T requestData, RequestCallBack<ObjectData> callback);

    public abstract void cancel();
}
