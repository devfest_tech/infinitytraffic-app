package com.tdstudio.infinitytraffic.base;

/**
 * Base
 * <p>
 * Copyright © 2016 BAP CO., LTD
 * Created by PHUQUY on 6/27/17.
 */

public interface RequestCallBack<R> {
    void onSuccess(R response);

    void onError(int code, String message);

    void cancel();
}
