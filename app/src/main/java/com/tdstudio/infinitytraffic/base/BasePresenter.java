package com.tdstudio.infinitytraffic.base;

/**
 * Created by bapvn on 08/08/2018.
 */

public abstract class BasePresenter<View extends BaseView, Interactor extends BaseInteractor,T,Object> implements RequestCallBack<Object> {
    private final View mView;
    private final Interactor mInteractor;

    public BasePresenter(View view, Interactor interactor) {
        mView = view;
        mInteractor = interactor;
    }

    protected View getView() {
        return mView;
    }

    protected Interactor getInteractor() {
        return mInteractor;
    }


    public abstract void request(T requestData);

    public abstract void cancel();

}
