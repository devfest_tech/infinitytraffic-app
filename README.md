# INFINITY TRAFFIC APP

## Requirements
- Android Smartphone: >= Marshmallow 6.0
- Download Deploygate App in CH Play Store
- 
```https://play.google.com/store/apps/details?id=com.deploygate```
- Scan QR Code through Deploygate App

![](https://deploygate.com/qr?data=https%3A%2F%2Fdeploygate.com%2Fdistributions%2F052e4103455104b27985b3714d20af2cdfc9560d&size=178)

## Detail Guide

### App help you knows the jam traffic locations in your city and suggests the routes avoids jam traffic

#### Screen 1: Get Jam Traffic Locations

![](https://scontent.fdad2-1.fna.fbcdn.net/v/t1.15752-9/72420807_780115249111409_6985670977304657920_n.jpg?_nc_cat=110&_nc_eui2=AeGvrHTIFCX9-vQiLO0nyh_15vISH0lqQJ_TbobG3J8iuUKrPGx_WDSYZOYmIBFiAxYhAweRcaGEHG2RzM_1WGAL4zAle1a8eE_UhgfeXm0NBA&_nc_oc=AQn_TXw2I5T6TQBKSVFkoitV_PmXg69vukyY6ce3QpaKyf2u7ZvK5HXrDy2FJiBfqAU&_nc_ht=scontent.fdad2-1.fna&oh=42411e3f09455f5213aa77455fb74917&oe=5E1F11ED)

#### Screen 2: Suggest routes from A to B avoids Jam Traffic

![](https://scontent.fdad2-1.fna.fbcdn.net/v/t1.15752-9/72230488_489649481883611_9123259291237613568_n.jpg?_nc_cat=110&_nc_eui2=AeFn_QQqSpauOLU6dNueFT8Qs81OINNXx11zKanymu7jJQZCjsOYnEeRFBXmKCaCkm0sGAOpqmYGSpMeRQC_7CsvJFw0aH-dMHAqflq7p45T0w&_nc_oc=AQnO_SSJeYs4fW_JfnYyOHVmszCx8AHL8tXB7tiHAY8Hlgm8ybcsi9xCwVDT-L9u6Sc&_nc_ht=scontent.fdad2-1.fna&oh=282c5205c32ce3ee6051b393a2f2d79a&oe=5E364834)